﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using SupGo.DAL.NetCore.Contexts;
using SupGo.DAL.NetCore.Extensions;

namespace SupGo.DAL.NetCore.Services
{
    public class DatabaseService<T> where T : class
    {
        /// <summary>
        /// Return all the elements of the table T
        /// </summary>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        public static IList<T> GetAll(bool doCleanId, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            using (var context = new SupGoDatabaseContext())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                if (doCleanId)
                    list = dbQuery
                        .AsNoTracking()
                        .CleanIdentifiers<T>()
                        .LoadChilds()
                        .ToList<T>();
                else 
                    list = dbQuery
                        .AsNoTracking()
                        .LoadChilds()
                        .ToList<T>();
            }
            return list;
        }

        /// <summary>
        /// Get a list of items from the table T filtered by an expression
        /// </summary>
        /// <param name="where"></param>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        public static IList<T> GetList(bool doCleanId, Func<T, bool> where, 
             params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            using (var context = new SupGoDatabaseContext())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                if(doCleanId)
                    list = dbQuery
                        .AsNoTracking()
                        .Where(where)
                        .CleanIdentifiers<T>()
                        .LoadChilds()
                        .ToList<T>();
                else
                    list = dbQuery
                        .AsNoTracking()
                        .Where(where)
                        .LoadChilds()
                        .ToList<T>();
            }
            return list;
        }

        /// <summary>
        /// Get a single object in the table T found by an expression
        /// </summary>
        /// <example>
        /// Category category = DatabaseService<Category>.GetSingle(c => c.Name == "Nourriture");
        /// </example>
        /// <param name="where"></param>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        public static T GetSingle(bool doCleanId, Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;
            using (var context = new SupGoDatabaseContext())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                if(doCleanId)
                    item = dbQuery.AsNoTracking().FirstOrDefault(where).CleanIdentifier<T>().LoadChilds();
                else
                    item = dbQuery.AsNoTracking().FirstOrDefault(where);
            }
            return item;
        }

        /// <summary>
        /// Add the item(s) in the table T
        /// </summary>
        /// <param name="items"></param>
        public static void Add(params T[] items)
        {
            using (var context = new SupGoDatabaseContext())
            {
                foreach (T item in items)
                {
                    try
                    {
                        context.Entry(item).State = EntityState.Added;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        /// <summary>
        /// Update the item(s) of the table T
        /// </summary>
        /// <param name="items"></param>
        public static void Update(params T[] items)
        {
            using (var context = new SupGoDatabaseContext())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Remove the item(s) of the table T
        /// </summary>
        /// <param name="items"></param>
        public static void Remove(params T[] items)
        {
            using (var context = new SupGoDatabaseContext())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }
    }
}
