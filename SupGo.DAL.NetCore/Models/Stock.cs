﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Stock
    {
        public int IdStock { get; set; }
        public string Uuid { get; set; }
        public int Quantity { get; set; }
        public string ProductUuid { get; set; }
        public string StoreUuid { get; set; }

        public Product ChildProduct { get; set; }
        public Store ChildStore { get; set; }
    }
}
