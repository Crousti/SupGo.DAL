﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Action
    {
        public Action()
        {
            History = new HashSet<History>();
        }

        public int IdAction { get; set; }
        public string Uuid { get; set; }
        public string ActionType { get; set; }

        public ICollection<History> History { get; set; }
    }
}
