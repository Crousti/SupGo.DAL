﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Discount
    {
        public Discount()
        {
            Orders = new HashSet<Orders>();
        }

        public int IdDiscount { get; set; }
        public string Uuid { get; set; }
        public int Percentage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProductUuid { get; set; }

        public Product ChildProduct { get; set; }
        public ICollection<Orders> Orders { get; set; }
    }
}
