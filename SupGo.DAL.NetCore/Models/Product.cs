﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Product
    {
        public Product()
        {
            Comment = new HashSet<Comment>();
            Discount = new HashSet<Discount>();
            History = new HashSet<History>();
            Orders = new HashSet<Orders>();
            Stock = new HashSet<Stock>();
        }

        public int IdProduct { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }
        public string CategoryUuid { get; set; }
        public string Image { get; set; }

        public Category ChildCategory { get; set; }
        public ICollection<Comment> Comment { get; set; }
        public ICollection<Discount> Discount { get; set; }
        public ICollection<History> History { get; set; }
        public ICollection<Orders> Orders { get; set; }
        public ICollection<Stock> Stock { get; set; }
    }
}
